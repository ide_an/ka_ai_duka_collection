-- vim: foldmethod=marker

-- {{{ debug
-- local fp = io.open(os.date("%Y%m%d%H%M%S").."-"..tostring(player_side).."p.log","w");
-- fp:setvbuf("no")
local prev_life = game_sides[player_side].player.life
local prev_choice = nil
local prev_x= 0
local prev_y= 0
local is_ex= false
local opponent_type = (function()
  local opponent_side
  if player_side == 1 then
    opponent_side = game_sides[2]
  else
    opponent_side = game_sides[1]
  end
  return opponent_side.player.character
end)()
local i_am_cirno = (game_sides[player_side].player.character == CharacterType.Cirno)
-- }}}

-- {{{ utility

local function adjustX(x)
  return math.max(-136, math.min(x, 136)) -- -136 <= x <= 136
end

local function adjustY(y)
  return math.max(16, math.min(y, 432)) -- 16 <= y <= 432
end

local function choosemin(candidates, func)
  local min = math.huge
  local min_i = -1
  for i, cnd in ipairs(candidates) do
    local v = func(cnd)
    if  v < min then
      min = v
      min_i = i
    end
  end
  return candidates[min_i]
end

local function findById(objects, id)
  for i, obj in ipairs(objects) do
    if obj.id == id then
      return obj
    end
  end
  return nil
end

-- パフォーマンスがあれならインスタンスを使いまわすようにする
local function copyHitBody(body)
  return {
    type = body.type,
    x = body.x,
    y = body.y,
    width = body.width,
    height = body.height,
    radius = body.radius,
    angle = body.angle
  }
end

local function copyHitBodyTo(body, allocated_body)
  allocated_body.type = body.type
  allocated_body.x = body.x
  allocated_body.y = body.y
  allocated_body.width = body.width
  allocated_body.height = body.height
  allocated_body.radius = body.radius
  allocated_body.angle = body.angle
  return allocated_body
end

local function vectorCosine(a, b)
  local inner_product = a.x * b.x + a.y * b.y
  return inner_product / math.sqrt(a.x ^ 2 + a.y ^ 2) / math.sqrt(b.x ^ 2 + b.y ^ 2)
end

local function squareDistance(a, b)
  return (a.x - b.x) ^ 2 + (a.y - b.y) ^ 2
end

local function cutOff(x, min, max)
  return math.min(math.max(x, min), max)
end
-- }}}

-- {{{ 当たり判定予測系
local prev_hit_bodies = {}
local function recordHitBodies(objects)
  for i,obj in ipairs(objects) do
    if obj.hitBody ~= nil then
      prev_hit_bodies[obj.id] = copyHitBody(obj.hitBody)
      prev_hit_bodies[obj.id].vx = obj.vx
      prev_hit_bodies[obj.id].vy = obj.vy
    end
  end
end

local function recordAllHitBodies(game_side)
  prev_hit_bodies = {}
  recordHitBodies(game_side.enemies)
  recordHitBodies(game_side.bullets)
  recordHitBodies(game_side.exAttacks)
end

local function predictHitBody(id, frame, current_body, vx, vy)
  local result_body = copyHitBody(current_body)
  local prev_body = prev_hit_bodies[id]
  if prev_body ~= nil then
    -- 移動の予測(等加速度で増加、ただし離散化している。)
    local ax = vx - prev_body.vx
    local ay = vy - prev_body.vy
    result_body.x= ax * frame * (frame + 1) / 2 + vx * frame + current_body.x
    result_body.y= ay * frame * (frame + 1) / 2 + vy * frame + current_body.y
    -- サイズの予測(等速で増加)
    result_body.width = (current_body.width - prev_body.width) * frame + current_body.width
    result_body.height = (current_body.height - prev_body.height) * frame + current_body.height
    result_body.radius = (current_body.radius - prev_body.radius) * frame + current_body.radius
    --{{{debug
    -- if is_ex then
    --   fp:write(
    --   "id:"..tostring(id)..
    --   --" ay:"..tostring(ay)..
    --   --" vy:"..tostring(vy)..
    --   " cur:"..tostring(current_body.x)..","..tostring(current_body.y)..
    --   --" y:"..tostring(current_body.y)..
    --   " pred:"..tostring(result_body.x)..","..tostring(result_body.y)..
    --   --" py:"..tostring(result_body.y)..
    --   " w:"..tostring(result_body.width)..
    --   " h:"..tostring(result_body.height).."\n")
    -- end
    -- }}}
  end
  return result_body
end

-- }}}

-- {{{ 当たり判定系

local function shouldPredict(obj_body, player_body)
  local prev_width, prev_height, prev_radius
  local size_rate = 10
  if obj_body.type ~= HitType.Circle then
    prev_width = player_body.width
    prev_height = player_body.height
    player_body.width = prev_width * size_rate
    player_body.height = prev_height * size_rate
  else
    prev_radius = player_body.radius
    player_body.radius = prev_radius * size_rate
  end
  local ret = hitTest(obj_body, player_body)
  if obj_body.type ~= HitType.Circle then
    player_body.width = prev_width
    player_body.height = prev_height
  else
    player_body.radius = prev_radius
  end
  return ret
end

local function shouldPredictExAttack(obj_body, player_body)
  if (obj_body.x - player_body.x) ^ 2 + (obj_body.y - player_body.y) ^ 2 < 200 ^ 2 then
    return true
  end
  return false
end

local function bitLarger(hitbody, allocated_body)
  copyHitBodyTo(hitbody, allocated_body)
  allocated_body.width = allocated_body.width * 1.5
  allocated_body.height = allocated_body.height * 1.5
  allocated_body.radius = allocated_body.radius * 1.5
  return allocated_body
end

local function calculateHitRisk(candidates, objects, player, should_predict, frame_max)
  local larger_body = {}
  for i, obj in ipairs(objects) do
    local obj_body = obj.hitBody
    if obj_body ~= nil then
      local player_body
      if obj_body.type == HitType.Circle then
        player_body = player.hitBodyCircle
      else
        player_body = player.hitBodyRect
      end
      if should_predict(obj_body, player_body) then
        for frame=1,frame_max do
          local tmp_body = predictHitBody(obj.id, frame, obj_body, obj.vx, obj.vy)
          larger_body = bitLarger(tmp_body, larger_body)
          for j, cnd in ipairs(candidates) do
            player_body.x = adjustX(player.x + frame * cnd.dx)
            player_body.y = adjustY(player.y + frame * cnd.dy)
            if hitTest(player_body, larger_body) then -- ちょっと大きめの判定で当たる場合も避けてほしいという思い
              if hitTest(player_body, tmp_body) then
                cnd.hitRisk = cnd.hitRisk + (1/2)^frame
                if frame == 1 then
                  cnd.willDie = true
                end
              else
                cnd.hitRisk = cnd.hitRisk + (1/2)^(frame + 1)
              end
            end
          end
          player_body.x = player.x
          player_body.y = player.y
        end
      end
    end
  end
end

local function calculateAllHitRisk(candidates, game_side)
  calculateHitRisk(candidates, game_side.enemies, game_side.player, shouldPredict, 5)
  calculateHitRisk(candidates, game_side.bullets, game_side.player, shouldPredict, 5)
  if opponent_type ~= CharacterType.Medicine and opponent_type ~= CharacterType.Medicine then
    calculateHitRisk(candidates, game_side.exAttacks, game_side.player, shouldPredictExAttack, 10)
  end
end

-- }}}

-- {{{ 吸霊系
local activate_spirit_threshold = 100 ^ 2
if i_am_cirno then
  activate_spirit_threshold = 50 ^ 2
end
local function shouldBeSlow(game_side, is_medicine_effect)
  if is_medicine_effect then
    -- 毒霧の中で悠長なこと言ってられん
    return false
  end
  -- 自機の周囲に活性化されてない幽霊が居れば吸霊フィールドを展開したい所存
  local player = game_side.player
  for i,enemy in ipairs(game_side.enemies) do
    if enemy.isSpirit and not(enemy.isActivatedSpirit) and squareDistance(player, enemy) < activate_spirit_threshold then
      return true
    end
  end
  return false
end
-- }}}

-- {{{ 操作候補

local function generateCandidates(player, is_slow, is_medicine_effect)
  local candidates = {}
  local dxs = {1, 0, -1, 0, 1}
  local dys = {0, 1, 0, -1, 0}
  -- キー入力。→、↓、←、↑、←
  local keys = {0x80,0x20,0x40,0x10,0x80}
  local speed = player.speedFast
  if is_slow then
    speed = player.speedSlow
  end
  if is_medicine_effect then
    speed = speed * 0.4
  end
  -- 停止
  candidates[1] = {
    key = 0,
    dx = 0,
    dy = 0,
    escapeFromExAttackCost = 0, -- EXアタックから逃げたい
    followingCost = 0, -- 追跡コスト
    hitRisk = 0, -- 被弾リスク
    willDie = false -- 次のフレームで死ぬか?
  }
  local sqrt_1_2 = 1 / math.sqrt(2)
  for i=1, #keys - 1 do
    local k = 2 * i
    candidates[k] = {
      key = keys[i],
      dx = speed * dxs[i],
      dy = speed * dys[i],
      escapeFromExAttackCost = 0, -- EXアタックから逃げたい
      followingCost = 0, -- 追跡コスト
      hitRisk = 0, -- 被弾リスク
      willDie = false -- 次のフレームで死ぬか?
    }
    -- 斜め
    candidates[k + 1] = {
      key = keys[i] + keys[i + 1],
      dx = sqrt_1_2 * speed * (dxs[i] + dxs[i + 1]),
      dy = sqrt_1_2 * speed * (dys[i] + dys[i + 1]),
      escapeFromExAttackCost = 0, -- EXアタックから逃げたい
      followingCost = 0, -- 追跡コスト
      hitRisk = 0, -- 被弾リスク
      willDie = false -- 次のフレームで死ぬか?
    }
  end
  return candidates
end

-- }}}

-- {{{ 追跡系

local function calculateFollowingCost(candidates, player, target)
  for i, cnd in ipairs(candidates) do
    local dx = 0
    local dy = 0
    if target ~= nil then
      dx = target.x - adjustX(player.x + cnd.dx)
      dy = target.y - adjustY(player.y + cnd.dy)
    end
    cnd.followingCost = math.sqrt(dx ^ 2 + dy ^ 2)
  end
end

local target_enemy_id = nil
local function getCurrentTargetEnemy(player, enemies)
  local target_enemy = findById(enemies, target_enemy_id)
  if target_enemy == nil then
    target_enemy = choosemin(enemies, function(enemy)
      -- 擬似的な敵やボスは無視
      if enemy.isPseudoEnemy or enemy.isBoss then
        return math.huge
      end
      if enemy.isSpirit then
        if enemy.isActivatedSpirit then
          -- 活性化した幽霊はさっさと殺したいという思い。
          return squareDistance(enemy, player) * 0.5
        else
          -- ただの幽霊は割と放置したいという思い。
          return squareDistance(enemy, player) * 4
        end
      end
      return squareDistance(enemy, player)
    end)
  end
  if target_enemy ~= nil then
    target_enemy_id = target_enemy.id
  else
    target_enemy_id = nil
  end
  return target_enemy
end

-- }}}

-- {{{ 目標点決定系
local function getCurrentTargetPosition(game_side, target_enemy)
  local player = game_side.player
  local y_min = 300
  local y_max = 432
  if opponent_type == CharacterType.Marisa then -- 汚いが、vs魔理沙はすこし前進してもらう
    y_min = 200
    y_max = 400
  end
  if #game_side.items > 0 then
    local nearest_item = choosemin(game_side.items, function(cnd)
      return squareDistance(cnd, player)
    end)
    return { x=nearest_item.x, y=cutOff(nearest_item.y, y_min, y_max)}
  elseif target_enemy then
    return { x=target_enemy.x, y=cutOff(target_enemy.y + 100, y_min, y_max)}
  end
  return nil
end
-- }}}

-- {{{ 攻撃系
local function shouldResetSpellPoint(game_side)
  if i_am_cirno then -- チルノは殴り続けたい
    return false
  end
  if #game_side.enemies > 20 then -- 敵倒すの優先したみ
    return false
  end
  if game_side.player.spellPoint > 500000 then
    return true
  end
  -- 自陣にボスが来てるかつ今のspellPointを増やすより100000ptを狙うほうが早い場合の処理
  if (game_side.player.spellPoint > 300000 and (500000 - game_side.player.spellPoint > 100000)) or
    (game_side.player.spellPoint > 100000 and (300000 - game_side.player.spellPoint > 100000)) then
    for i,enemy in ipairs(game_side.enemies) do
      if enemy.isBoss then
        return true
      end
    end
  end
  return false
end
local function shouldShoot(game_side, choice)
  -- TODO: もっと賢く
  if shouldResetSpellPoint(game_side) and choice.hitRisk < 0.01 then
    return false
  end
  return true
end

local function shouldBomb(game_side, choice)
  if choice.willDie and game_side.player.currentChargeMax >= 200 then
    return true
  elseif #game_side.items > 0 and game_side.player.currentChargeMax >= 200 then
    -- Gアイテムが降ってきてるときはボムってしまえ
    for i,item in ipairs(game_side.items) do
      if item.type == ItemType.G then
        return true
      end
    end
  end
  return false
end

local function shouldCharge(game_side, choice)
  -- TODO: チャージアタックのスケジューリング
  -- 自機周囲の弾の数に応じてボムるとか
  local player = game_side.player
  if player.currentChargeMax >= 300 and player.currentCharge < 200 then
     return true
  end
  if i_am_cirno and player.currentChargeMax >= 200 and player.currentCharge < 200 then
     return true
  end
  return false
end

local generateShootKey = coroutine.wrap(function ()
  while true do
    coroutine.yield(1)
    coroutine.yield(0)
  end
end)
-- }}}

-- {{{ EXアタック対策系
local function calculateEscapeFromExAttackCost(candidates, game_side)
  local player =game_side.player
  if opponent_type == CharacterType.Marisa then
    -- 魔理沙は恐ろしい生き物だ
    local player_body = copyHitBody(player.hitBodyRect)
    --fp:write("----\n")
    for i,v in ipairs(game_side.exAttacks) do
      -- 予告線は取れないので、EXの位置から決め打ち
      local body = {
        type=HitType.RotatableRect,
        x=v.x,
        y=v.y,
        width=400,
        height=20,
        angle=math.pi * 3 / 2
      }
      for j,cnd in ipairs(candidates) do
        player_body.x = adjustX( player.x + cnd.dx * 5)
        player_body.y = adjustY( player.y + cnd.dy * 5)
        if hitTest(player_body, body) then
          --fp:write("lazer will hit"..tostring(v.x)..","..tostring(v.y).."\n")
          cnd.escapeFromExAttackCost = cnd.escapeFromExAttackCost + 10
        end
      end
    end
  else
    local pos = {}
    -- とりあえずEXアタックから距離を置きたい
    for i,v in ipairs(game_side.exAttacks) do
      for j,cnd in ipairs(candidates) do
        pos.x = adjustX( player.x + cnd.dx * 5)
        pos.y = adjustY( player.y + cnd.dy * 5)
        local d = squareDistance(pos, v)
        if d < 50^2 then -- FIXME: 雑
          cnd.escapeFromExAttackCost = cnd.escapeFromExAttackCost + 1
        end
      end
    end
  end
end

local function checkMedicineEffect(player, ex_attacks)
  if opponent_type ~= CharacterType.Medicine then
    return false
  end
  for i,v in ipairs(ex_attacks) do
    if v.hitBody and hitTest(player.hitBodyCircle, v.hitBody) then
      return true
    end
  end
  return false
end
-- }}}

function main()
  local myside = game_sides[player_side]
  local player = myside.player
  local is_medicine_effect = checkMedicineEffect(player, myside.exAttacks)
  local is_slow = shouldBeSlow(myside, is_medicine_effect)
  local candidates = generateCandidates(player, is_slow, is_medicine_effect)
  local target_enemy = getCurrentTargetEnemy(player, myside.enemies)
  -- {{{ debug
  -- if target_enemy ~= nil then
  --   fp:write(tostring(frame)..": target:"..tostring(target_enemy.id).." pos:"..tostring(target_enemy.x)..","..tostring(target_enemy.y).."\n")
  -- else
  --   fp:write(tostring(frame)..": target: none\n")
  -- end
  -- }}}
  local target_position = getCurrentTargetPosition(myside, target_enemy)
  if target_position then
    --fp:write(tostring(frame)..": targetpos:"..tostring(target_position.x)..","..tostring(target_position.y).."\n")
    calculateFollowingCost(candidates, player, target_position)
  end
  calculateAllHitRisk(candidates, myside)
  calculateEscapeFromExAttackCost(candidates, myside)
  recordAllHitBodies(myside) -- 次回の予測用に記録
  --calculateLeaveFromHomePositionCost(candidates, )

  local choice = choosemin(candidates, function (candidate)
    return candidate.hitRisk * 10000 + candidate.followingCost + candidate.escapeFromExAttackCost
  end)
  -- {{{debug
  -- if player.life < prev_life then
  --   fp:write(
  --   "died:hitRisk:"..tostring(prev_choice.hitRisk)..
  --   " willDie:"..tostring(prev_choice.willDie)..
  --   " life:"..tostring(player.life)..
  --   " pos:"..tostring(prev_x)..","..tostring(prev_y)..
  --   " pred_pos:"..tostring(adjustX(prev_x + prev_choice.dx))..","..tostring(adjustY(prev_y + prev_choice.dy))..
  --   " cur_pos:"..tostring(player.x)..","..tostring(player.y)..
  --   " r:"..tostring(player.hitBodyCircle.radius)..
  --   "\n")
  -- end
  -- }}}

  -- {{{ キー生成
  local keys = choice.key
  if shouldBomb(myside, choice) then
    -- fp:write("bomb:hitRisk:"..tostring(choice.hitRisk).." willDie:"..tostring(choice.willDie).."\n")
    keys = keys + 0x2
  end
  -- ボムりながら撃つ自由もあるわけで
  if shouldCharge(myside, choice) then
    keys = keys + 0x1
  elseif shouldShoot(myside, choice) then
      keys = keys + generateShootKey()
  end
  if is_slow then
    keys = keys + 0x4
  end
  -- }}}
  sendKeys(keys)
  -- {{{debug
  prev_choice = choice
  prev_life = player.life
  prev_x = player.x
  prev_y = player.y
  -- }}}
end

